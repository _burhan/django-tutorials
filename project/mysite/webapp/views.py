from django.shortcuts import render

# Create your views here.


def index(request):
    return render(request, "webapp/home.html")


def contact(request):
    return render(request,  'webapp/basic.html',
                  {'content': ['If you enjoy python join us send us a message @',
                               'http"//www.meetup.com/Kuwait-Python-Programming-Group/']})
